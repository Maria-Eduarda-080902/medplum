FROM node:16-slim

ENV NODE_ENV=development
WORKDIR .
RUN rm -rf ./node_modules
COPY package.json .
RUN npm install --quiet
COPY . .
WORKDIR ./packages/server
COPY ./packages/server/ .
COPY ./packages/server/package.json .
RUN ls
RUN npm ci
RUN npm run dev
RUN echo ^C

COPY . .
WORKDIR ./packages/app
COPY ./packages/app/ .
COPY ./packages/app/package.json .
RUN ls
RUN npm ci
RUN npm run dev


